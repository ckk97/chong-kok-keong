﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models.ModelClass
{
    public class UserProperty
    {
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("Password")]
        public string Password { get; set; }
        [DisplayName("Full Name")]
        public string FullName { get; set; }
        [DisplayName("Email Address")]
        public string Email { get; set; }
        [DisplayName("Mobile No")]
        public string MobileNo { get; set; }
        [DisplayName("Role")]
        public int? RoleId { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
        [DisplayName("Group ID")]
        public int? GroupId { get; set; }
        public int? ParentId { get; set; }

        public int? Level { get; set; }

    }
}

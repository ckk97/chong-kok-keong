﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models.ModelClass
{
    public class LoginProperty
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

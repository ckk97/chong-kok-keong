﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class ComplianceProperty: LogDataRequest
    {
        public string OrderId { get; set; }
        public int ProviderNo { get; set; }
        public int Settle { get; set; }
        public DateTime? BetDateFrom { get; set; }
        public DateTime? BetDateTo { get; set; }
        public DateTime? SettlementDateFrom { get; set; }
        public DateTime? SettlementDateTo { get; set; }
        public int? ReportType { get; set; }
        public string ProductType { get; set; }
        public string Provider { get; set; }
        public int? Status { get; set; }
        public byte[] ProfileImage { get; set; }
        public string ImageFileType { get; set; }
        public int CartType { get; set; }
        public int DateDiff { get; set; }
        public string GameId { get; set; }
    }
}

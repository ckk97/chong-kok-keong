﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class GeneralProperty
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string ProjectList { get; set; }
        public string ListId { get; set; }
        public int? UpdateBy { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public int? ProjectId { get; set; }
        public int? TypeId { get; set; }
        public int? PageSize { get; set; }
        public int? PageNo { get; set; }
        public string AccessList { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public int? Id { get; set; }
        public string Code { get; set; }
        public int? LanguageId { get; set; } = 1;
        public string IP { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public Guid? Token { get; set; }
    }
}

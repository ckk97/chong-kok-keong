﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class LogDataRequest : GeneralProperty
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string SearchString { get; set; }
        public string LogLevel { get; set; }
    }
    public class LogDataResponse
    {
        public DateTime LogDateTime { get; set; }
        public string LogLevel { get; set; }
        public string IP { get; set; }
        public string Identifier { get; set; }
        public string LogMessage { get; set; }
    }
}

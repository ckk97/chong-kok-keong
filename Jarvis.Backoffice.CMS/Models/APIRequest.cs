﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class APIRequest
    {
        public SystemAccess Access { get; set; }
        public ComplianceProperty Property { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class LoginProperty
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

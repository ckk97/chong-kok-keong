﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Models
{
    public class SystemAccess
    {
        public int? TypeId { get; set; }
        public string Password { get; set; }
        public Guid? Token { get; set; } = null;
        public int? FunctionId { get; set; }
        public string Parameter { get; set; }
        public string URL { get; set; }
        public string IP { get; set; }
    }
}

﻿using Jarvis.Backoffice.CMS.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.CustomException
{
    public class InvalidAccessException : Exception
    {
        public InvalidAccessException()
            : base("Invalid system access for the current user.")
        {
            base.Data.Add("ErrorCode", ErrorCode.InvalidAccess);
        }

        public InvalidAccessException(string message)
            : base(string.Format("Invalid system access for the current user. {0}", message))
        {
            base.Data.Add("ErrorCode", ErrorCode.InvalidAccess);
        }

        public InvalidAccessException(Exception ex)
            : base("Invalid system access for the current user")
        {
            base.Data.Add("ErrorCode", ErrorCode.InvalidAccess);
            base.Data.Add("Data", ex.Message);
        }
    }
}

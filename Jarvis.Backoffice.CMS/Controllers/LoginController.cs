﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Jarvis.Backoffice.CMS.Enum;
using Jarvis.Backoffice.CMS.General;
using Jarvis.Backoffice.CMS.Models.ModelClass;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jarvis.Backoffice.CMS.Controllers
{
    [Route("")]
    public class LoginController : Controller
    {
        _GeneralFunction _cls_GeneralFunction = new _GeneralFunction();
        [HttpGet]
        [Route("")]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(LoginProperty login)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand sqlCmd = new SqlCommand();
                //sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                //sqlCmd.CommandText = "SP_AdminMS_AdminLogin";
                sqlCmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = login.UserName;
                sqlCmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _GeneralFunction.Fn_Sha512Hash(login.Password);

                ds=_IntegrationTools.GetDataSet(DBConnCategory.MainConn, "SP_AdminMS_UserLogin", sqlCmd);

                if(ds.Tables[0].Rows.Count > 0)
                {
                    HttpContext.Session.SetString("UserId", ds.Tables[0].Rows[0]["Id"].ToString());
                    HttpContext.Session.SetString("UserToken", ds.Tables[0].Rows[0]["Token"].ToString());
                    HttpContext.Session.SetString("GroupId", ds.Tables[0].Rows[0]["GroupId"].ToString());
                    HttpContext.Session.SetString("Level", ds.Tables[0].Rows[0]["Level"].ToString());
                    return RedirectToAction("Home", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }
    }
}

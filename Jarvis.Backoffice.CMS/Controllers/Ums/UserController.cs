﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Jarvis.Backoffice.CMS.Enum;
using Jarvis.Backoffice.CMS.General;
using Jarvis.Backoffice.CMS.Models.ModelClass;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Jarvis.Backoffice.CMS.Controllers.User_Management_Controller
{
    public class UserController : Controller
    {
        [HttpGet]
        public IActionResult User(UserProperty user)
        {
            string query = "select * from AdminMS_User WITH(NOLOCK)";

            var ds = _IntegrationTools.GetDataSet(DBConnCategory.MainConn, query);
            var convertedList = (from rw in ds.Tables[0].AsEnumerable()
                                 select new UserProperty()
                                 {
                                     UserName = Convert.ToString(rw["UserName"]),
                                     //Password = Convert.ToString(rw["Password"]),
                                     FullName = Convert.ToString(rw["FullName"]),
                                     Email = Convert.ToString(rw["Email"]),
                                     MobileNo = Convert.ToString(rw["MobileNo"]),
                                     RoleId = rw["RoleId"] == DBNull.Value ? null : (int?)Convert.ToInt32(rw["RoleId"]),
                                     GroupId = rw["GroupId"] == DBNull.Value ? null : (int?)Convert.ToInt32(Convert.ToInt32(rw["GroupId"])),
                                     ParentId = rw["ParentId"] == DBNull.Value ? null : (int?)Convert.ToInt32(Convert.ToInt32(rw["ParentId"])),
                                     Level = rw["Level"] == DBNull.Value ? null : (int?)Convert.ToInt32(Convert.ToInt32(rw["Level"]))
                                 }).ToList();

            return View(convertedList);
        }
        public IActionResult Create()
        {
            
            return View(new UserProperty() { Roles = GetRole() });
        }
        [HttpPost]
        public IActionResult Create(UserProperty user)
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = user.UserName;
            sqlCmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.Password;
            sqlCmd.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = user.FullName;
            sqlCmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = user.Email;
            sqlCmd.Parameters.Add("@MobileNo", SqlDbType.NVarChar).Value = user.MobileNo;
            sqlCmd.Parameters.Add("@RoleId", SqlDbType.Int).Value = user.RoleId;
            sqlCmd.Parameters.Add("@GroupId", SqlDbType.Int).Value = (object)user.GroupId ?? DBNull.Value;
            sqlCmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            //sqlCmd.Parameters.Add("@Level", SqlDbType.Int).Value = user.Level;
            sqlCmd.Parameters.Add("@Status", SqlDbType.Int).Value = 1;
            sqlCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            sqlCmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            
            if (_IntegrationTools.ExecuteQuery(DBConnCategory.MainConn, "SP_UserMS_AddUser", sqlCmd)==1)
            {
                TempData["notification"] = "Test";
            }

            return RedirectToAction("User", "User");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                StudentDBHandle sdb = new StudentDBHandle();
                if (sdb.DeleteStudent(id))
                {
                    ViewBag.AlertMsg = "Student Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public IEnumerable<SelectListItem> GetRole()
        {
            string query = "select ID, RoleName from AdminMS_Role WITH(NOLOCK)";
            var ds = _IntegrationTools.GetDataSet(DBConnCategory.MainConn, query);
            var ddl_roleList = (from rw in ds.Tables[0].AsEnumerable()
                                select new SelectListItem()
                                {
                                    Value = Convert.ToString(rw["ID"]),
                                    Text = Convert.ToString(rw["RoleName"])

                                }).ToList();
            var ddl_Role = new SelectListItem()
            {
                Value = null,
                Text = "--- select role ---"
            };

            ddl_roleList.Insert(0, ddl_Role);
            return new SelectList(ddl_roleList, "Value", "Text");
        }
    }
}

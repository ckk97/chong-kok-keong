﻿using Jarvis.Backoffice.CMS.CustomException;
using Jarvis.Backoffice.CMS.Enum;
using Jarvis.Backoffice.CMS.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace Jarvis.Backoffice.CMS.General
{
    public class _IntegrationTools
    {
        private static void OpenConnection(DBConnCategory dbType, ref SqlConnection MyConn)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(System.IO.Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();

            string strConn = string.Empty;
            switch (dbType)
            {
                case DBConnCategory.MainConn:
                    strConn = configuration.GetConnectionString("connStr");
                    break;
                case DBConnCategory.SubConn:
                    strConn = configuration.GetConnectionString("connStr2");
                    break;
                case DBConnCategory.CQ9Conn:
                    strConn = configuration.GetConnectionString("connStr3");
                    break;
            }
            MyConn = new SqlConnection(strConn);
            MyConn.Open();
        }

        public static DataSet GetDataSet(DBConnCategory dbCategory, string sSP_Name, SqlCommand cmd_getData)
        {
            SqlDataAdapter obj_getData = new SqlDataAdapter();
            DataSet ds_getData = new DataSet("Dataset");
            SqlConnection MyConn = null;
            try
            {
                OpenConnection(dbCategory, ref MyConn);
                cmd_getData.Connection = MyConn;
                cmd_getData.CommandText = sSP_Name;
                cmd_getData.CommandType = CommandType.StoredProcedure;
                cmd_getData.CommandTimeout = 600;
                obj_getData.SelectCommand = cmd_getData;
                obj_getData.Fill(ds_getData);
                return ds_getData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd_getData.Dispose();
                CloseConnection(ref MyConn);
            }
        }

        public static DataSet GetDataSet(DBConnCategory dbCategory, string query)
        {
            SqlDataAdapter obj_getData = new SqlDataAdapter();
            DataSet ds_getData = new DataSet("Dataset");
            SqlConnection MyConn = null;
            SqlCommand cmd_getData = new SqlCommand();
            try
            {
                OpenConnection(dbCategory, ref MyConn);
                cmd_getData.Connection = MyConn;
                cmd_getData.CommandText = query;
                cmd_getData.CommandType = CommandType.Text;
                cmd_getData.CommandTimeout = 600;
                obj_getData.SelectCommand = cmd_getData;
                obj_getData.Fill(ds_getData);
                return ds_getData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd_getData.Dispose();
                CloseConnection(ref MyConn);
            }
        }

        public static void ExecuteQuery(DBConnCategory dbCategory, string sSP_Name)
        {
            SqlConnection MyConn = null;
            try
            {
                OpenConnection(dbCategory, ref MyConn);
                SqlCommand cmd_getData = MyConn.CreateCommand();
                cmd_getData.CommandText = sSP_Name;
                cmd_getData.CommandType = CommandType.StoredProcedure;
                cmd_getData.CommandTimeout = 600;
                cmd_getData.ExecuteNonQuery();
                cmd_getData.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection(ref MyConn);
            }
        }

        public static int ExecuteQuery(DBConnCategory dbCategory, string sSP_Name, SqlCommand cmd_getData)
        {
            SqlConnection MyConn = null;
            try
            {
                OpenConnection(dbCategory, ref MyConn);
                cmd_getData.Connection = MyConn;
                cmd_getData.CommandText = sSP_Name;
                cmd_getData.CommandType = CommandType.StoredProcedure;
                cmd_getData.CommandTimeout = 600;
                int i = cmd_getData.ExecuteNonQuery();
                cmd_getData.Dispose();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection(ref MyConn);
            }
        }

        public static void ExecuteQueryLock(DBConnCategory dbCategory, string sSP_Name, SqlCommand cmd_getData)
        {
            SqlConnection MyConn = null;
            SqlTransaction transaction;

            OpenConnection(dbCategory, ref MyConn);

            transaction = MyConn.BeginTransaction();

            try
            {
                cmd_getData.Connection = MyConn;
                cmd_getData.CommandText = sSP_Name;
                cmd_getData.CommandType = CommandType.StoredProcedure;
                cmd_getData.CommandTimeout = 600;
                cmd_getData.Transaction = transaction;
                cmd_getData.ExecuteNonQuery();
                cmd_getData.Dispose();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                CloseConnection(ref MyConn);
            }
        }

        private static void CloseConnection(ref SqlConnection MyConn)
        {
            MyConn.Dispose();
        }

        public static void Fn_SystemAccessChecking(SystemAccess access)
        {
            access.Parameter = "";
            Type fieldsType = access.GetType();

            foreach (PropertyInfo prop in fieldsType.GetProperties())
            {
                if (prop.GetValue(access, null) != null)
                {
                    if ((access.Parameter == ""))
                    {
                        access.Parameter = (prop.Name + (" = " + prop.GetValue(access, null).ToString()));
                    }
                    else
                    {
                        access.Parameter = (access.Parameter + (" // " + (prop.Name + (" = " + prop.GetValue(access, null).ToString()))));
                    }
                }
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Parameters.Add("@Type", SqlDbType.Int).Value = (object)access.TypeId ?? DBNull.Value;
            sqlCmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = access.Password;
            sqlCmd.Parameters.Add("@Token", SqlDbType.UniqueIdentifier).Value = access.Token;
            sqlCmd.Parameters.Add("@FunctionId", SqlDbType.Int).Value = (object)access.FunctionId ?? DBNull.Value;
            sqlCmd.Parameters.Add("@Parameter", SqlDbType.NVarChar).Value = access.Parameter;
            sqlCmd.Parameters.Add("@URL", SqlDbType.NVarChar).Value = access.URL;
            sqlCmd.Parameters.Add("@IP", SqlDbType.NVarChar).Value = access.IP;

            try
            {
                GetDataSet(DBConnCategory.MainConn, "SP_SystemMS_APIAccessChecking", sqlCmd);
            }
            catch (SqlException ex)
            {
                throw new InvalidAccessException(ex);
            }
        }
    }
}

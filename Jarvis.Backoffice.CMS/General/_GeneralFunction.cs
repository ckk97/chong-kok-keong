﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Jarvis.Backoffice.CMS.General
{
    public class _GeneralFunction
    {
        public static string Fn_Sha512Hash(string text)
        {
            SHA512CryptoServiceProvider shaObj = new SHA512CryptoServiceProvider();
            byte[] hash = System.Text.Encoding.Unicode.GetBytes(text);
            hash = shaObj.ComputeHash(hash);
            return System.Text.Encoding.Unicode.GetString(hash);
        }

        public static string GenerateFileName(string context)
        {
            return context + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Guid.NewGuid().ToString("N");
        }

        public static string GenerateFileName(string context, string ext)
        {
            return context + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + Guid.NewGuid().ToString("N") + "." + ext;
        }

        //public static void SystemSendEmail(int EmailId, int? LanguageId, GeneralProperty property)
        //{
        //    try
        //    {
        //        SqlCommand sqlCmd = new SqlCommand();
        //        sqlCmd.Parameters.Add("@Id", SqlDbType.Int).Value = EmailId;
        //        sqlCmd.Parameters.Add("@LanguageId", SqlDbType.Int).Value = (object)LanguageId ?? DBNull.Value;
        //        DataSet ds_Email = _IntegrationTools.GetDataSet(DBConnCategory.MainConn, "SP_SystemMS_GetEmail", sqlCmd);

        //        IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
        //        IConfiguration configuration = builder.Build();
        //        SMTP_Settings smtp_Settings = new SMTP_Settings();
        //        Mail_Settings mail_Settings = new Mail_Settings();
        //        configuration.GetSection("SMTP_Settings").Bind(smtp_Settings);
        //        configuration.GetSection("Mail_Settings").Bind(mail_Settings);

        //        SmtpClient client = new SmtpClient();
        //        client.Port = smtp_Settings.SMTP_Port;
        //        client.Host = smtp_Settings.SMTP_Host;
        //        client.EnableSsl = true;
        //        client.Timeout = 10000;
        //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        client.UseDefaultCredentials = false;
        //        client.Credentials = new NetworkCredential(smtp_Settings.SMTP_Account, DESDecrypt(smtp_Settings.SMTP_Password));

        //        string subject = ds_Email.Tables[0].Rows[0]["Subject"].ToString();
        //        string content = ds_Email.Tables[0].Rows[0]["Content"].ToString();

        //        content = content.Replace("{0}", mail_Settings.Web_Url);
        //        content = content.Replace("{1}", property.FullName);
        //        content = content.Replace("{3}", mail_Settings.Company_Name);
        //        content = content.Replace("{4}", DateTime.Now.Year.ToString());
        //        content = content.Replace("{5}", "");


        //        if (EmailId == 1)
        //        {
        //            content = content.Replace("{2}", (mail_Settings.Web_Url + ("AdminResetPassword.aspx?Code=" + property.Code)));
        //        }

        //        MailMessage mm = new MailMessage(mail_Settings.Sender_Email, property.Email, subject, content);
        //        mm.BodyEncoding = UTF8Encoding.UTF8;
        //        mm.IsBodyHtml = true;
        //        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        //        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

        //        client.Send(mm);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        static byte[] secretkey = ASCIIEncoding.ASCII.GetBytes("Goku");

        public static string DESEncrypt(string originalString)
        {
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(secretkey, secretkey), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string DESDecrypt(string cryptedString)
        {
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(secretkey, secretkey), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }

        public static readonly Logger _logger = new LoggerConfiguration()
                                                        .ReadFrom.Configuration(new ConfigurationBuilder()
                                                        .SetBasePath(Directory.GetCurrentDirectory())
                                                        .AddJsonFile("appsettings.json")
                                                        .Build())
                                                        .CreateLogger();

        public string Fn_ConvertTableRowToString(DataTable dt_RawData, String str_IdName)
        {
            String str_Variable = "";

            for (int int_Count = 0; int_Count < dt_RawData.Rows.Count; int_Count++)
            {
                if (str_Variable == "")
                {
                    str_Variable = dt_RawData.Rows[int_Count][str_IdName].ToString();
                }
                else
                {
                    str_Variable = str_Variable + ',' + dt_RawData.Rows[int_Count][str_IdName].ToString();
                }
            }

            return str_Variable;
        }
    }
}

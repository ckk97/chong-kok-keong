﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Enum
{
    public enum DBConnCategory
    {
        MainConn = 1,
        SubConn = 2,
        CQ9Conn = 3
    }
}

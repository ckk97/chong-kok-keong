﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jarvis.Backoffice.CMS.Enum
{
    public enum ErrorCode
    {
        Success = 0,
        InvalidAccess = 1,
        SQLError = 2,
        SystemError = 3,
        FileOperationFailed = 4
    }
}
